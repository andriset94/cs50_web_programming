from flask import Flask, render_template, request
import datetime

app = Flask(__name__)

@app.route("/")
def index():
    now = datetime.datetime.now()
    new_year = now.month == 1 and now.day == 1
    return render_template("index.html",new_year=new_year)

@app.route("/names/<string:name>")
def hello(name):
    return f"Hello, {name}"

@app.route("/names")
def names():
    names = ["Alice","Bob","Charlie"]
    return render_template("index.html",names=names)

@app.route("/more")
def more():
    return render_template("more.html")

@app.route("/second_page")
def second_page():
    return render_template("index_test.html")

@app.route("/forms")
def forms():
    return render_template("forms.html")

@app.route("/hello",methods=["GET","POST"])
def hi():
    if request.method == "GET":
        return "<a href='forms'>Click Here</a>"
    else:
        name = request.form.get("name")
        return render_template("hello.html",name=name)
